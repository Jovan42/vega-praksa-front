import {
  GET_CLIENTS,
  CLIENTS_SEARCH,
  CLIENTS_SEARCH_BY_FIRST_LETTER,
  GET_PROJECTS,
  PROJECTS_SEARCH,
  PROJECTS_SEARCH_BY_FIRST_LETTER,
  GET_COUNTRIES,
  CLIENT_EDIT,
  CLIENT_DELETE,
  CLIENT_ADD
} from "../constants/action-types";

export function getClients() {
  return { type: GET_CLIENTS };
}

export function editClient(client) {
  return { type: CLIENT_EDIT, client: client };
}

export function deleteClient(client) {
  return { type: CLIENT_DELETE, client: client  };
}

export function addClient(client) {
  return { type: CLIENT_ADD, client: client  };
}

export function searchClients(name) {
  return { type: CLIENTS_SEARCH, name: name };
}

export function searchByFirstLetter(letter) {
  return { type: CLIENTS_SEARCH_BY_FIRST_LETTER, letter: letter };
}

export function getProjects() {
  return { type: GET_PROJECTS };
}

export function searchProjects(name) {
  return { type: PROJECTS_SEARCH, name: name };
}

export function searchProjectsByFirstLetter(letter) {
  return { type: PROJECTS_SEARCH_BY_FIRST_LETTER, letter: letter };
}

export function getCountries() {
  return { type: GET_COUNTRIES };
}

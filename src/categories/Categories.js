import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import NewCategory from "./NewCategory";
import Category from "./Category";

class Categories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      categories: null,
      overlayHiden: true
    };
  }

  toggleOverlay = () => {
    this.setState({
      overlayHiden: !this.state.overlayHiden
    });
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/categories")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            categories: result.categories
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, categories } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="content">
          {!this.state.overlayHiden && (
            <NewCategory toggleOverlay={this.toggleOverlay} />
          )}
          <div onClick={this.toggleOverlay}>
            <p className="new">Create new Category</p>
          </div>
          <div>
            {categories.map(item => (
              <Category key={item.id}item={item} />
            ))}
          </div>
        </div>
      );
    }
  }
}

export default Categories;

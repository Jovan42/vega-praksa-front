import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

class EditCateogry extends React.Component {
  save = () => {
    fetch("http://localhost:8080/api/categories", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: this.props.item.id,
        name: this.refs.editName.value
      })
    });
    window.location.reload();
  };

  delete = () => {
    fetch("http://localhost:8080/api/categories/" + this.props.item.id, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
    });
    window.location.reload();
  };

  render() {
    return (
      <div className="edit">
        <form>
          <div className="form-group">
            <label>Name</label>
            <input
              defaultValue={this.props.item.name}
              className="form-control"
              ref="editName"
              placeholder="Enter name"
            />
          </div>
          <button
            onClick={this.save}
            type="button"
            className="btn btn-success overlaybtn"
          >
            Save
          </button>
          <button
            onClick={this.delete}
            type="button"
            className="btn btn-danger overlaybtn"
          >
            Delete
          </button>
        </form>
      </div>
    );
  }
}
export default EditCateogry;

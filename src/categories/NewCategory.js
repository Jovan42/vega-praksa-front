import React from "react";

class NewCategory extends React.Component {
  addCategory = () => {
    fetch("http://localhost:8080/api/categories", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: this.refs.name.value
      })
    });
    window.location.reload();
  };

  render() {
    return (
      <div id="overlay">
        <div className="form-group overlay-From">
          <form className="mx-auto inputDiv">
            <div className="">
              <label>Name</label>
              <input
                ref="name"
                className="mx-auto form-control"
                placeholder="Name"
              />
            </div>
            <button
              onClick={this.addCategory}
              type="button"
              className="btn btn-success overlaybtn"
            >
              Add
            </button>
            <button
              onClick={this.props.toggleOverlay}
              type="button"
              className="btn btn-danger overlaybtn"
            >
              Cancel
            </button>
          </form>
        </div>
      </div>
    );
  }
}
export default NewCategory;

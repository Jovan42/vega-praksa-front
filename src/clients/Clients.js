import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import NewClient from "./NewClient";
import Client from "./Client";
import Alphabet from "../templates/Alphabet";
import { connect } from "react-redux";
import {
  getClients,
  searchClients,
  searchByFirstLetter
} from "../actions/index";

class Clients extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      categories: null,
      overlayHiden: true,
      letter: null
    };
  }
  selectLetter = atr => {
    if (this.refs.picked != null) {
      this.refs.picked.className = "";
      this.refs = {
        picked: ""
      };
    }
    if (this.state.letter === atr.target.letter) {
      this.setState({
        letter: null
      });
    } else {
      this.state.letter = atr.target.innerHTML;
      atr.target.className = "liPicked";
      this.refs = {
        picked: atr.target.parentNode
      };
    }

    this.props.searchByFirstLetter(this.state.letter);
  };

  toggleOverlay = () => {
    this.setState({
      overlayHiden: !this.state.overlayHiden
    });
  };

  search = () => {
    if (this.refs.search.value !== null && this.refs.search.value !== "")
      this.props.searchClients(this.refs.search.value);
    else this.props.getClients();
  };

  componentDidMount() {
    this.props.getClients();
  }

  render() {
    return (
      <div className="content">
        {!this.state.overlayHiden && (
          <NewClient toggleOverlay={this.toggleOverlay} />
        )}
        <div className="new">
          <p className=" inline" onClick={this.toggleOverlay}>
            Create new Client
          </p>
          <input
            className="search"
            ref="search"
            onChange={this.search}
            placeholder="Search"
          />
        </div>
        <Alphabet selectLetter={this.selectLetter} />
        <div>
          {this.props.clients.map(item => (
            <Client key={item.id} item={item} />
          ))}
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    clients: state.clients
  };
}

const mapDispatchToProps = dispatch => {
  return {
    getClients: () => dispatch(getClients()),
    searchClients: name => dispatch(searchClients(name)),
    searchByFirstLetter: name => dispatch(searchByFirstLetter(name))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Clients);

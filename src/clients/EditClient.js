import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { connect } from "react-redux";
import { getCountries, editClient, deleteClient } from "../actions/index";

class EditClient extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      categories: null,
      overlayHiden: true
    };
  }
  componentDidMount() {
    this.props.getCountries();
  }

  save = () => {
    var countryId = this.refs.editCountry.options[
      this.refs.editCountry.selectedIndex
    ].value;
    const client = {
      id: this.props.item.id,
      name: this.refs.editName.value,
      address: this.refs.editAddress.value,
      city: this.refs.editCity.value,
      zipCode: this.refs.editZipCode.value,
      country: countryId
    };
    this.props.editClient(client);
    window.location.reload();
  };

  delete = () => {
    this.props.deleteClient(this.props.item.id);
    window.location.reload();
  };

  render() {
    return (
      <div className="form-group overlay-From">
        <form className="mx-auto inputDiv">
          <h5>Create new team member</h5>
          <div className="form-group">
            <label>Name</label>
            <input
              defaultValue={this.props.item.name}
              className="form-control"
              ref="editName"
              placeholder="Name"
            />
          </div>
          <div className="form-group">
            <label>Address</label>
            <input
              defaultValue={this.props.item.address}
              className="form-control"
              ref="editAddress"
              placeholder="Address"
            />
          </div>
          <div className="form-group">
            <label>City</label>
            <input
              defaultValue={this.props.item.city}
              className="form-control"
              ref="editCity"
              placeholder="City"
            />
          </div>
          <div className="form-group">
            <label>Zip code</label>
            <input
              defaultValue={this.props.item.zipCode}
              className="form-control"
              ref="editZipCode"
              placeholder="Zip Code"
            />
          </div>
          <div className="form-group">
            <label className="radioLabel">Country</label>
            <select ref="editCountry" className="form-control">
              {this.props.countries.map(item => (
                <option
                  value={item.id}
                  selected={item.id === this.props.item.country.id}
                >
                  {item.name}
                </option>
              ))}
            </select>
          </div>
          <button
            onClick={this.save}
            type="button"
            className="btn btn-success overlaybtn"
          >
            Save
          </button>
          <button
            onClick={this.delete}
            type="button"
            className="btn btn-danger overlaybtn"
          >
            Delete
          </button>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    countries: state.countries
  };
}

const mapDispatchToProps = dispatch => {
  return {
    getCountries: () => dispatch(getCountries()),
    editClient: client => dispatch(editClient(client)),
    deleteClient: client => dispatch(deleteClient(client))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditClient);

import React from "react";
import { connect } from "react-redux";
import { addClient, getCountries } from "../actions/index";

class NewClient extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      categories: null,
      overlayHiden: true
    };
  }

  addTeamMember = () => {
    var countryId = this.refs.editCountry.options[
      this.refs.editCountry.selectedIndex
    ].value;
    const client = {
      name: this.refs.editName.value,
      address: this.refs.editAddress.value,
      city: this.refs.editCity.value,
      zipCode: this.refs.editZipCode.value,
      country: countryId
    };

    this.props.addClient(client);
 
    window.location.reload();
  };

  componentDidMount() {
    this.props.getCountries();
  }

  render() {
    return (
      <div id="overlay">
        <div className="form-group overlay-From">
          <form className="mx-auto inputDiv">
            <h5>Create new team member</h5>
            <div className="form-group">
              <label>Name</label>
              <input
                className="form-control"
                ref="editName"
                placeholder="Name"
              />
            </div>
            <div className="form-group">
              <label>Address</label>
              <input
                className="form-control"
                ref="editAddress"
                placeholder="Address"
              />
            </div>
            <div className="form-group">
              <label>City</label>
              <input
                className="form-control"
                ref="editCity"
                placeholder="City"
              />
            </div>
            <div className="form-group">
              <label>Zip code</label>
              <input
                className="form-control"
                ref="editZipCode"
                placeholder="Zip Code"
              />
            </div>
            <div className="form-group">
              <label className="radioLabel">Country</label>
              <select ref="editCountry" className="form-control">
                {this.props.countries.map(item => (
                  <option value={item.id}>{item.name}</option>
                ))}
              </select>
            </div>
            <button
              onClick={this.addTeamMember}
              type="button"
              className="btn btn-success overlaybtn"
            >
              Add
            </button>
            <button
              onClick={this.props.toggleOverlay}
              type="button"
              className="btn btn-danger overlaybtn"
            >
              Cancel
            </button>
          </form>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    countries: state.countries
  };
}

const mapDispatchToProps = dispatch => {
  return {
    getCountries: () => dispatch(getCountries()),
    addClient: client => dispatch(addClient(client))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewClient);

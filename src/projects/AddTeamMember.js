import React from "react";

class AddTeamMember extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      employees: null,
    };
  }

  addTeamMember = ()=>{
    var emId = this.refs.editEm.options[this.refs.editEm.selectedIndex]
    .value;
    fetch("http://localhost:8080/api/project-members", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          project: this.props.item.id,
          employee: emId,
          dailyAllocation: this.refs.editDaily.value
        })
      });
      window.location.reload();
  }

  componentDidMount() {
    fetch("http://localhost:8080/api/team-members/no-project")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            employees: result.teamMembers
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded} = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div id="overlay">
          <div className="form-group overlay-From">
            <form className="mx-auto inputDiv">
              <h5>Add Team member</h5>
              <div className="form-group">
                <label>Employee </label>
                <select  className="form-control" ref="editEm">
                {this.state.employees.map(item => (
                  <option key={item.id} value={item.id}>{item.name}</option>
                ))}
              </select>
              </div>
              <div className="form-group">
              <label>Daily Allocation</label>
              <input
                className="form-control"
                ref="editDaily"
                placeholder="Name"
              />
            </div>
              <button
                onClick={this.addTeamMember}
                type="button"
                className="btn btn-success overlaybtn"
              >
                Add
              </button>
              <button
                onClick={this.props.toggleOverlay}
                type="button"
                className="btn btn-danger overlaybtn"
              >
                Cancel
              </button>
            </form>
          </div>
        </div>
      );
    }
  }
}

export default AddTeamMember;

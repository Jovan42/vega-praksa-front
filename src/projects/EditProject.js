import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import AddTeamMember from "./AddTeamMember";
import TeamMember from "./TeamMember";


class EditProject extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      teamMembers: null,
      clients: null,
      overlayHiden: true,
      addTeamMemberOverlayHiden: true
    };
  }
  toggleaddTeamMemberOverlayHiden= () => {
    this.setState({
      addTeamMemberOverlayHiden: !this.state.addTeamMemberOverlayHiden
    });
  };

  
  componentDidMount() {
    fetch("http://localhost:8080/api/team-members")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            teamMembers: result.teamMembers
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );

    fetch("http://localhost:8080/api/clients")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedClietns: true,
            clients: result.clients
          });
        },
        error => {
          this.setState({
            isLoadedClietns: true,
            error
          });
        }
      );
  }
  save = () => {
    var leadId = this.refs.editLead.options[this.refs.editLead.selectedIndex]
      .value;
    var clientId = this.refs.editClient.options[
      this.refs.editClient.selectedIndex
    ].value;

    fetch("http://localhost:8080/api/projects", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: this.props.item.id,
        name: this.refs.editName.value,
        description: this.refs.editDesc.value,
        lead: leadId,
        client: clientId
      })
    });
    window.location.reload();
  };

  delete = () => {
    fetch("http://localhost:8080/api/projects/" + this.props.item.id, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
    });
    window.location.reload();
  };

  render() {
    const { error, isLoaded, isLoadedClietns } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded || !isLoadedClietns) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="form-group overlay-From">
         {!this.state.addTeamMemberOverlayHiden && (
            <AddTeamMember item={this.props.item} toggleOverlay={this.toggleaddTeamMemberOverlayHiden} />
          )}
          <form className="mx-auto inputDiv">
            <h5>Create new project</h5>
            <div className="form-group">
              <label>Name</label>
              <input
                defaultValue={this.props.item.name}
                className="form-control"
                ref="editName"
                placeholder="Name"
              />
            </div>
            <div className="form-group">
              <label>Description</label>
              <input
                defaultValue={this.props.item.description}
                className="form-control"
                ref="editDesc"
                placeholder="Description"
              />
            </div>
            <div className="form-group">
              <label className="radioLabel">Lead</label>
              <select ref="editLead" className="form-control">
                {this.state.teamMembers.map(item => (
                  <option key={item.id} value={item.id}>{item.name}</option>
                ))}
              </select>
            </div>
            <div className="form-group">
              <label className="radioLabel" >Clients</label>
              <select ref="editClient"  className="form-control">
                {this.state.clients.map(item => (
                  <option key={item.id} value={item.id}>{item.name}</option>
                ))}
              </select>
            </div>
            <div className="form-group">
                <label className="radioLabel">Team members</label>
                
                <button 
                onClick={this.toggleaddTeamMemberOverlayHiden}
                type="button"
                className="btn btn-success overlaybtn"
                >
                +
              </button>
              
            <div className="form-group">
            <label className="radioLabel">Project members</label>
            {this.props.item.projectMembers.map(item => (
            <TeamMember key={item.id} item={item}/>
            
                ))}
            </div>
              </div>
            <button
              onClick={this.save}
              type="button"
              className="btn btn-success overlaybtn"
            >
              Save
            </button>
            <button
              onClick={this.delete}
              type="button"
              className="btn btn-danger overlaybtn"
            >
              Delete
            </button>
          </form>
        </div>
      );
    }
  }
}
export default EditProject;

import React from "react";

class NewProject extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      isLoadedClietns: false,
      overlayHiden: true
    };
  }

  addTeamMember = () => {
    var leadId = this.refs.editLead.options[this.refs.editLead.selectedIndex]
      .value;
    var clientId = this.refs.editClient.options[
      this.refs.editClient.selectedIndex
    ].value;

    fetch("http://localhost:8080/api/projects", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: this.refs.editName.value,
        description: this.refs.editDesc.value,
        lead: leadId,
        client:  clientId
      })
    });
    //window.location.reload();
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/team-members")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            teamMembers: result.teamMembers
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );

    fetch("http://localhost:8080/api/clients")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedClietns: true,
            clients: result.clients
          });
        },
        error => {
          this.setState({
            isLoadedClietns: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, isLoadedClietns } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded || !isLoadedClietns) {
      return <div>Loading...</div>;
    } else {
      return (
        <div id="overlay">
          <div className="form-group overlay-From">
            <form className="mx-auto inputDiv">
              <h5>Create new project</h5>
              <div className="form-group">
                <label>Name</label>
                <input
                  className="form-control"
                  ref="editName"
                  placeholder="Name"
                />
              </div>
              <div className="form-group">
                <label>Description</label>
                <input
                  className="form-control"
                  ref="editDesc"
                  placeholder="Description"
                />
              </div>
              <div className="form-group">
                <label className="radioLabel">Lead</label>
                <select ref="editLead" className="form-control">
                  {this.state.teamMembers.map(item => (
                    <option key={item.id} value={item.id}>{item.name}</option>
                  ))}
                </select>
              </div>
              <div className="form-group">
                <label className="radioLabel">Clients</label>
                <select ref="editClient" className="form-control">
                  {this.state.clients.map(item => (
                    <option key={item.id} value={item.id}>{item.name}</option>
                  ))}
                </select>
              </div>

              <button
                onClick={this.addTeamMember}
                type="button"
                className="btn btn-success overlaybtn"
              >
                Add
              </button>
              <button
                onClick={this.props.toggleOverlay}
                type="button"
                className="btn btn-danger overlaybtn"
              >
                Cancel
              </button>
            </form>
          </div>
        </div>
      );
    }
  }
}
export default NewProject;

import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import NewProject from "./NewProject";
import Project from "./Project";
import Alphabet from "../templates/Alphabet";
import { connect } from "react-redux";
import {
  getProjects,
  searchProjects,
  searchProjectsByFirstLetter
} from "../actions/index";

class Projects extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      projects: null,
      overlayHiden: true,
      letter: null
    };
  }
  selectLetter = atr => {
    if (this.refs.picked != null) {
      this.refs.picked.className = "";
      this.refs = {
        picked: ""
      };
    }
    if (this.state.letter === atr.target.letter) {
      this.setState({
        letter: null
      });
    } else {
      this.state.letter = atr.target.innerHTML;
      atr.target.className = "liPicked";
      this.refs = {
        picked: atr.target.parentNode
      };
    }

    this.props.searchProjectsByFirstLetter(this.state.letter);
  };

  toggleOverlay = () => {
    this.setState({
      overlayHiden: !this.state.overlayHiden
    });
  };

  search = () => {
    if (this.refs.search.value !== null && this.refs.search.value !== "")
      this.props.searchProjects(this.refs.search.value);
    else this.props.getProjects();
  };

  componentDidMount() {
    this.props.getProjects();
  }

  render() {
    return (
      <div className="content">
        {!this.state.overlayHiden && (
          <NewProject toggleOverlay={this.toggleOverlay} />
        )}
        <div className="new">
          <p className=" inline" onClick={this.toggleOverlay}>
            Create new Client
          </p>
          <input
            className="search"
            ref="search"
            onChange={this.search}
            placeholder="Search"
          />
        </div>
        <Alphabet selectLetter={this.selectLetter} />
        <div>
          {this.props.projects.map(item => (
            <Project key={item.id} item={item} />
          ))}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    projects: state.projects
  };
}

const mapDispatchToProps = dispatch => {
  return {
    getProjects: () => dispatch(getProjects()),
    searchProjects: name => dispatch(searchProjects(name)),
    searchProjectsByFirstLetter: name =>
      dispatch(searchProjectsByFirstLetter(name))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Projects);

import React from "react";

class TeamMember extends React.Component {

    removeProjectMember = () => {
        fetch("http://localhost:8080/api/project-members/" + this.props.item.id, {
            method: "DELETE",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json"
            }
          });
          window.location.reload();
    }
  render() {
    return (
      <div>
        <h2 className="d-inline" key={this.props.item.id} value={this.props.item.id}>
          {this.props.item.employee.name}
        </h2>
        <p className="d-inline" > ({this.props.item.dailyAllocation} hrs)</p>
        <button
          onClick={this.removeProjectMember}
          type="button"
          className="btn btn-danger overlaybtn d-inline"
        >
          x
        </button>
      </div>
    );
  }
}
export default TeamMember;

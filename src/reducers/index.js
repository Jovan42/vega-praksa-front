import {
  CLIENTS_LOADED,
  API_ERROR,
  PROJECTS_LOADED,
  COUNTRIES_LOADED
} from "../constants/action-types";

const initialState = {
  clients: [],
  projects: [],
  countries: []
};
function rootReducer(state = initialState, action) {
  if (action.type === CLIENTS_LOADED) {
    return Object.assign({}, state, {
      clients: action.payload.clients
    });
  }
  if (action.type === PROJECTS_LOADED) {
    return Object.assign({}, state, {
      projects: action.payload.projects
    });
  }
  if (action.type === COUNTRIES_LOADED) {
    return Object.assign({}, state, {
      countries: action.payload.countries
    });
  }
  if (action.type === API_ERROR) {
  }
  return state;
}
export default rootReducer;

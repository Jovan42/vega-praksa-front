import { takeEvery, call, put } from "redux-saga/effects";
import {
  GET_CLIENTS,
  API_ERROR,
  CLIENTS_LOADED,
  CLIENTS_SEARCH,
  CLIENTS_SEARCH_BY_FIRST_LETTER,
  PROJECTS_LOADED,
  PROJECTS_SEARCH,
  PROJECTS_SEARCH_BY_FIRST_LETTER,
  GET_PROJECTS,
  GET_COUNTRIES,
  COUNTRIES_LOADED,
  CLIENT_EDIT,
  CLIENT_DELETE,
  CLIENT_ADD
} from "../constants/action-types";

export default function* getClientsWatcher() {
  yield takeEvery(GET_CLIENTS, getClientsWorker);
  yield takeEvery(CLIENTS_SEARCH, searchClientsWorker);
  yield takeEvery(CLIENT_DELETE, deleteClientWorker);
  yield takeEvery(
    CLIENTS_SEARCH_BY_FIRST_LETTER,
    searchClientsByFirstLetterWorker
  );
  yield takeEvery(CLIENT_EDIT, clientEditWorker);
  yield takeEvery(CLIENT_ADD, clientAddWorker);

  yield takeEvery(GET_PROJECTS, getProjectsWorker);
  yield takeEvery(PROJECTS_SEARCH, searchProjectsWorker);
  yield takeEvery(
    PROJECTS_SEARCH_BY_FIRST_LETTER,
    searchProjectsByFirstLetterWorker
  );

  yield takeEvery(GET_COUNTRIES, getCountriesWorker);
}

function* getClientsWorker() {
  try {
    const payload = yield call(getClients);
    yield put({ type: CLIENTS_LOADED, payload });
  } catch (e) {
    yield put({ type: API_ERROR, payload: e });
  }
}

function* clientAddWorker(action) {
  try {
    const payload = yield call(addClient, action.client);
  } catch (e) {
    yield put({ type: API_ERROR, payload: e });
  }
}

function* searchClientsWorker(action) {
  try {
    const payload = yield call(searchClients, action);
    yield put({ type: CLIENTS_LOADED, payload });
  } catch (e) {
    yield put({ type: API_ERROR, payload: e });
  }
}

function* deleteClientWorker(action) {
  try {
    const payload = yield call(deleteClient, action);
  } catch (e) {
    yield put({ type: API_ERROR, payload: e });
  }
}

function* searchClientsByFirstLetterWorker(action) {
  try {
    const payload = yield call(searchClientsByFirstLetter, action);
    yield put({ type: CLIENTS_LOADED, payload });
  } catch (e) {
    yield put({ type: API_ERROR, payload: e });
  }
}

function* getProjectsWorker() {
  try {
    const payload = yield call(getProjects);
    yield put({ type: PROJECTS_LOADED, payload });
  } catch (e) {
    yield put({ type: API_ERROR, payload: e });
  }
}

function* searchProjectsWorker(action) {
  try {
    const payload = yield call(searchProjects, action);
    yield put({ type: PROJECTS_LOADED, payload });
  } catch (e) {
    yield put({ type: API_ERROR, payload: e });
  }
}

function* clientEditWorker(action) {
  try {
    const payload = yield call(editClient, action.client);
    yield put({ type: PROJECTS_LOADED, payload });
  } catch (e) {
    yield put({ type: API_ERROR, payload: e });
  }
}

function* getCountriesWorker() {
  try {
    const payload = yield call(getCountries);
    yield put({ type: COUNTRIES_LOADED, payload });
  } catch (e) {
    yield put({ type: API_ERROR, payload: e });
  }
}

function* searchProjectsByFirstLetterWorker(action) {
  try {
    const payload = yield call(searchProjectsByFirstLetter, action);
    yield put({ type: PROJECTS_LOADED, payload });
  } catch (e) {
    yield put({ type: API_ERROR, payload: e });
  }
}

function getClients() {
  return fetch("http://localhost:8080/api/clients").then(response =>
    response.json()
  );
}

function getProjects() {
  return fetch("http://localhost:8080/api/projects").then(response =>
    response.json()
  );
}

function getCountries() {
  return fetch("http://localhost:8080/api/countries").then(response =>
    response.json()
  );
}

function deleteClient(action) {
  return fetch("http://localhost:8080/api/clients/" + action.client, {
    method: "DELETE"
  });
}

function searchClients(action) {
  return fetch(
    "http://localhost:8080/api/clients/by-name?name=" + action.name
  ).then(response => response.json());
}

function searchClientsByFirstLetter(action) {
  return fetch(
    "http://localhost:8080/api/clients/first-letter?letter=" + action.letter
  ).then(response => response.json());
}

function searchProjects(action) {
  return fetch(
    "http://localhost:8080/api/projects/by-name?name=" + action.name
  ).then(response => response.json());
}

function searchProjectsByFirstLetter(action) {
  return fetch(
    "http://localhost:8080/api/projects/first-letter?letter=" + action.letter
  ).then(response => response.json());
}

function editClient(client) {

  fetch("http://localhost:8080/api/clients", {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      id: client.id,
      name: client.name,
      address: client.address,
      city: client.city,
      zipCode: client.zipCode,
      country: client.country
    })
  });
}

function addClient(client) {
  fetch("http://localhost:8080/api/clients", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      name: client.name,
      address: client.address,
      city: client.city,
      zipCode: client.zipCode,
      country: client.country
    })
  });
}

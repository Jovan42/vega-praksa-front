import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

class EditTeamMember extends React.Component {
  save = () => {
    var role = "WORKER";
    if (this.refs.admin.checked) role = "ADMIN";

    var status = "ACTIVE";
    if (this.refs.inactive.checked) status = "INACTIVE";

    fetch("http://localhost:8080/api/team-members", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: this.props.item.id,
        name: this.refs.editName.value,
        hoursPerWeek: this.refs.editHours.value,
        username: this.refs.editUsername.value,
        email: this.refs.editEmail.value,
        role: role,
        status: status
      })
    });
    window.location.reload();
  };

  delete = () => {
    fetch("http://localhost:8080/api/team-members/" + this.props.item.id, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
    });
    window.location.reload();
  };

  render() {
    var admin = this.props.item.role === "ADMIN";
    var inactive = this.props.item.status === "INACTIVE";

    return (
      <div className="edit">
        <form>
          <h5>Create new team member</h5>
          <div className="form-group">
            <label>Name</label>
            <input
              defaultValue={this.props.item.name}
              className="form-control"
              ref="editName"
              placeholder="Name"
            />
          </div>
          <div className="form-group">
            <label>Hourse per week:</label>
            <input
              defaultValue={this.props.item.hoursPerWeek}
              className="form-control"
              ref="editHours"
              placeholder="Hourse per week"
            />
          </div>
          <div className="form-group">
            <label>Username</label>
            <input
              defaultValue={this.props.item.username}
              className="form-control"
              ref="editUsername"
              placeholder="Username"
            />
          </div>
          <div className="form-group">
            <label>Email</label>
            <input
              defaultValue={this.props.item.email}
              className="form-control"
              ref="editEmail"
              placeholder="Email"
            />
          </div>
          <div className="form-group">
            <label>Status</label>
            <div>
              <label className="radioLabel">Active</label>
              <input
                className="radioButton"
                type="radio"
                name="status"
                value="ACTIVE"
                defaultChecked="true"
              />
              <label className="radioLabel">Inactive</label>
              <input
                className="radioButton"
                type="radio"
                name="status"
                ref="inactive"
                value="INACTIVE"
                defaultChecked={inactive}
              />
            </div>
          </div>

          <div className="form-group">
            <label>Role</label>
            <div>
              <label className="radioLabel">Worker</label>
              <input
                className="radioButton"
                type="radio"
                name="role"
                value="WORKER"
                defaultChecked="true"
              />
              <label className="radioLabel">Admin</label>
              <input
                className="radioButton"
                type="radio"
                name="role"
                ref="admin"
                value="ADMIN"
                defaultChecked={admin}
              />
            </div>
          </div>
          <button
            onClick={this.save}
            type="button"
            className="btn btn-success overlaybtn"
          >
            Save
          </button>
          <button
            onClick={this.delete}
            type="button"
            className="btn btn-danger overlaybtn"
          >
            Delete
          </button>
        </form>
      </div>
    );
  }
}
export default EditTeamMember;

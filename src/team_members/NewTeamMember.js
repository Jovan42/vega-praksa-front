import React from "react";

class NewTeamMember extends React.Component {
  addTeamMember = () => {
    var role = "WORKER";
    if (this.refs.admin.checked) role = "ADMIN";

    var status = "ACTIVE";
    if (this.refs.inactive.checked) status = "INACTIVE";
    fetch("http://localhost:8080/api/team-members", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: this.refs.editName.value,
        hoursPerWeek: this.refs.editHours.value,
        username: this.refs.editUsername.value,
        email: this.refs.editEmail.value,
        password: this.refs.editPassword.value,
        role: role,
        status: status
      })
    });
    window.location.reload();
  };

  render() {
    return (
      <div id="overlay">
        <div className="form-group overlay-From">
          <form className="mx-auto inputDiv">
            <div className="form-group">
              <label>Name</label>
              <input
                className="form-control"
                ref="editName"
                placeholder="Name"
              />
            </div>
            <div className="form-group">
              <label>Hourse per week:</label>
              <input
                className="form-control"
                ref="editHours"
                placeholder="Hourse per week"
              />
            </div>
            <div className="form-group">
              <label>Username</label>
              <input
                className="form-control"
                ref="editUsername"
                placeholder="Username"
              />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input
                className="form-control"
                ref="editPassword"
                placeholder="Password"
              />
            </div>
            <div className="form-group">
              <label>Email</label>
              <input
                className="form-control"
                ref="editEmail"
                placeholder="Email"
              />
            </div>
            <div className="form-group">
              <label>Status</label>
              <div>
                <label className="radioLabel">Active</label>
                <input
                  className="radioButton"
                  type="radio"
                  name="status"
                  value="ACTIVE"
                  defaultChecked="true"
                />
                <label className="radioLabel">Inactive</label>
                <input
                  className="radioButton"
                  type="radio"
                  name="status"
                  ref="inactive"
                  value="INACTIVE"
                />
              </div>
            </div>

            <div className="form-group">
              <label>Role</label>
              <div>
                <label className="radioLabel">Worker</label>
                <input
                  className="radioButton"
                  type="radio"
                  name="role"
                  value="WORKER"
                  defaultChecked="true"
                />
                <label className="radioLabel">Admin</label>
                <input
                  className="radioButton"
                  type="radio"
                  name="role"
                  ref="admin"
                  value="ADMIN"
                />
              </div>
            </div>
            <button
              onClick={this.addTeamMember}
              type="button"
              className="btn btn-success overlaybtn"
            >
              Add
            </button>
            <button
              onClick={this.props.toggleOverlay}
              type="button"
              className="btn btn-danger overlaybtn"
            >
              Cancel
            </button>
          </form>
        </div>
      </div>
    );
  }
}
export default NewTeamMember;

import React from "react";
import plus from "../icons/acc-open.png";
import EditTeamMember from "./EditTeamMember";

class TeamMember extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false
    };
  }

  toggleEdit = () => {
    this.setState({
      edit: !this.state.edit
    });
  };

  render() {
    return (
      <div key={this.props.item.id} className="item">
        <div onClick={this.toggleEdit}>
          <span>
            {this.props.item.name}
            <img className="plus" src={plus} alt="plus" />
          </span>
        </div>
        {this.state.edit && <EditTeamMember item={this.props.item} />}
      </div>
    );
  }
}
export default TeamMember;

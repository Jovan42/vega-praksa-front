import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import TeamMember from "./TeamMember";
import NewTeamMember from "./NewTeamMember";

class TeamMembers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      categories: null,
      overlayHiden: true
    };
  }

  toggleOverlay = () => {
    this.setState({
      overlayHiden: !this.state.overlayHiden
    });
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/team-members")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            teamMembers: result.teamMembers
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, teamMembers } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="content">
          {!this.state.overlayHiden && (
            <NewTeamMember toggleOverlay={this.toggleOverlay} />
          )}
          <div onClick={this.toggleOverlay}>
            <p className="new">Create new Team Member</p>
          </div>
          <div>
            {teamMembers.map(item => (
              <TeamMember key={item.id} item={item} />
            ))}
          </div>
        </div>
      );
    }
  }
}

export default TeamMembers;

import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import NewMember from "./NewMember";
import Member from "./Member";

class EditTeam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      overlayHiden: true,
      allEmployees: null
    };
  }
  toggleOverlay = () => {
    this.setState({
      overlayHiden: !this.state.overlayHiden
    });
  };

  delete = () => {
    fetch("http://localhost:8080/api/teams/" + this.props.item.id, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
    });
    window.location.reload();
  };

  save = () => {
    var leaderId = this.refs.leader.options[
      this.refs.leader.selectedIndex
    ].value;
    var employeesids = this.props.item.employees.map(item => item.id);
    fetch("http://localhost:8080/api/teams", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: this.props.item.id,
        name: this.refs.editName.value,
        teamLeader: leaderId,
        employees: employeesids
      })
    });
    window.location.reload();
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/teams/" + this.props.item.id)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            teams: result.teams
          });
          console.log(result);
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );

      fetch("http://localhost:8080/api/team-members")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedAllEmp: true,
            allEmployees: result.teamMembers
          });
        },
        error => {
          this.setState({
            isLoadedAllEmp: true,
            error
          });
        }
      );
  }
  render() {
    const { error, isLoaded,isLoadedAllEmp } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded || !isLoadedAllEmp) {
      return <div>Loading...</div>;
    } else {
    return (
      <div className="edit">
        {!this.state.overlayHiden && (
          <NewMember
            item={this.props.item}
            toggleOverlay={this.toggleOverlay}
          />
        )}
        <form>
          <div className="form-group">
            <label>Name</label>
            <input
              defaultValue={this.props.item.name}
              className="form-control"
              ref="editName"
              placeholder="Name"
            />
          </div>
          <div className="form-group">
            <label>Team Leader: </label>
            <select ref="leader" className="form-control">
                {this.state.allEmployees.map(item => (
                  <option
                    value={item.id}
                    selected={item.id === this.props.item.teamLeader.id}
                  >
                    {item.name}
                  </option>
                ))}
              </select>
          </div>
          <div className="form-group">
            <label>Team members: </label>
            {this.props.item.employees.map(item => (
              <Member item={item} team={this.props.item} />
            ))}
          </div>

          <div>
            <button
              type="button"
              onClick={this.toggleOverlay}
              className="btn btn-success overlaybtn float-right"
            >
              +
            </button>
          </div>
          <button
            onClick={this.save}
            type="button"
            className="btn btn-success overlaybtn"
          >
            Save
          </button>
          <button
            onClick={this.delete}
            type="button"
            className="btn btn-danger overlaybtn"
          >
            Delete
          </button>
        </form>
      </div>
    );
    
  }
}
}
export default EditTeam;

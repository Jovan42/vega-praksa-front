import React from "react";

class Member extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      overlayHiden: true
    };
  }

  del = () => {
    fetch(
      "http://localhost:8080/api/teams/" +
        this.props.team.id +
        "/employees/" +
        this.props.item.id,
      {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      }
    );
    window.location.reload();
  };
  render() {
    return (
      <div key={this.props.item.id}>
        <h2 className="d-inline" key={this.props.item.id}>
          {this.props.item.name}{" "}
        </h2>
        <button className="btn btn-danger sd" onClick={this.del}>
          x
        </button>
      </div>
    );
  }
}
export default Member;

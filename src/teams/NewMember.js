import React from "react";

class NewMember extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      employees: null,
      isLoaded: false
    };
  }

  componentDidMount() {
    console.log(this.props.item)

    fetch("http://localhost:8080/api/team-members")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            employees: result.teamMembers
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }
  add = () => {
    var member = this.refs.addMember.options[
        this.refs.addMember.selectedIndex
      ].value;
    fetch("http://localhost:8080/api/teams/" + this.props.item.id + "/employees/" + member, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      
    });
    window.location.reload();
  };

  render() {
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
    return (
      <div id="overlay">
        <div className="form-group overlay-From">
          <form className="mx-auto inputDiv">
        <h3>Add new team member</h3>

          <div className="form-group">
              <label className="radioLabel">Country</label>
              <select ref="addMember"className="form-control">
                {this.state.employees.map(item => (
                  <option
                    value={item.id}
                    key={item.id}
                  >
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
            <button
              onClick={this.add}
              type="button"
              className="btn btn-success overlaybtn"
            >
              Add
            </button>
            <button
              onClick={this.props.toggleOverlay}
              type="button"
              className="btn btn-danger overlaybtn"
            >
              Cancel
            </button>
          </form>
        </div>
      </div>
    );
                }
  }
}
export default NewMember;

import React from "react";

class NewProject extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      employees: null,
      overlayHiden: true
    };
  }
  componentDidMount() {
    fetch("http://localhost:8080/api/team-members")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            employees: result.teamMembers
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }
  addTeam = () => {
    var leaderId = this.refs.leader.options[
      this.refs.leader.selectedIndex
    ].value;
    fetch("http://localhost:8080/api/teams", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: this.refs.editName.value,
        teamLeader: leaderId,
        employees:[]
      })
    });
    window.location.reload();
  };

  render() {
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
    return (
      <div id="overlay">
        <div className="form-group overlay-From">
          <form className="mx-auto inputDiv">
          <div className="form-group">
              <label>Name</label>
              <input
                className="form-control"
                ref="editName"
                placeholder="Name"
              />
            </div>
            <div className="form-group">
              <label className="radioLabel">Team leader</label>
              <select ref="leader" className="form-control">
                {this.state.employees.map(item => (
                  <option
                    value={item.id}
                  >
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
            <button
              onClick={this.addTeam}
              type="button"
              className="btn btn-success overlaybtn"
            >
              Add
            </button>
            <button
              onClick={this.props.toggleOverlay}
              type="button"
              className="btn btn-danger overlaybtn"
            >
              Cancel
            </button>
          </form>
        </div>
      </div>
    );
  }
}
}
export default NewProject;

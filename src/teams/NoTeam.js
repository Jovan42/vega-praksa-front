import React from "react";
import plus from "../icons/acc-open.png";
import ShowWithoutTeam from "./ShowWithoutTeam";

class NoTeam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false
    };
  }

  toggleEdit = () => {
    this.setState({
      edit: !this.state.edit
    });
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/team-members/no-team")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            teams: result.teams
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    return (
      <div className="item">
        <div onClick={this.toggleEdit}>
          <span>
            Employees without team
            <img className="plus" src={plus} alt="plus" />
          </span>
        </div>
        {this.state.edit && <ShowWithoutTeam item={this.props.item} />}
      </div>
    );
  }
}
export default NoTeam;

import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

class ShowWithoutTeam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      isLoaded: false,
      employees: null
    };
  }
  componentDidMount() {
    fetch("http://localhost:8080/api/team-members/no-team")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            employees: result.teamMembers
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }
  render() {
    const { error, isLoaded, employees } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="edit">
          {employees.map(item => (
              <h2 key={item.id}  className="form-control"> {item.name}</h2>
            ))}
        </div>
      );
    }
  }
}
export default ShowWithoutTeam;

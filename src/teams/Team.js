import React from "react";
import plus from "../icons/acc-open.png";
import EditTeam from "./EditTeam";

class Team extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      overlayHiden: true
    };
  }

  toggleEdit = () => {
    this.setState({
      edit: !this.state.edit
    });
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/teams")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            teams: result.teams
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    return (
      <div key={this.props.item.id} className="item">
        <div onClick={this.toggleEdit}>
          <span>
            {this.props.item.name}
            <img className="plus" src={plus} alt="plus" />
          </span>
        </div>
        {this.state.edit && <EditTeam item={this.props.item} />}
      </div>
    );
  }
}
export default Team;

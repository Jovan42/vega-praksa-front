import React from "react";
import NewTeam from "./NewTeam";
import Team from "./Team";
import NoTeam from "./NoTeam";

class Teams extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      overlayHiden: true,
      teams: null,
      error: null,
      isLoaded: false,
      user: null
    };
  }
  toggleOverlay = () => {
    this.setState({
      overlayHiden: !this.state.overlayHiden
    });
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/teams")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            teams: result.teams
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }
  render() {
    const { error, isLoaded, teams } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="content">
          {!this.state.overlayHiden && (
            <NewTeam toggleOverlay={this.toggleOverlay} />
          )}
          <div className="new">
            <p className=" inline" onClick={this.toggleOverlay}>
              Create new Team
            </p>
            
          </div>
          <div>
          <NoTeam />
            {teams.map(item => (
              <Team key={item.id} item={item} />
            ))}
          </div>
        </div>
      );
    }
  }
}
export default Teams;

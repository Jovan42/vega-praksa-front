import categoriesIcon from "../icons/categories.png";
import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Categories from "../categories/Categories";
import TeamMembers from "../team_members/TeamMembers";
import Clients from "../clients/Clients";
import Projects from "../projects/Projects";
import Reports from "../works/Reports";
import TimeSheet from "../time_sheet/TimeSheet";
import Day from "../time_sheet/Day";
import Teams from "../teams/Teams";
import Vacation from "../vacations/Vacation";
import Timeline from "../timeline/Timeline";

import { BrowserRouter as Router, Route } from "react-router-dom";

class Content extends React.Component {
  render() {
    return (
      <div className="content">
        <div className="title">
          <img className="smallIcon" src={categoriesIcon} alt="logo" />
          <h3 className="inline"> {this.props.title}</h3>
          <Router>
            <Route path="/clients" component={Clients} />
            <Route path="/reports" component={Reports} />
            <Route path="/projects" component={Projects} />
            <Route path="/categories" component={Categories} />
            <Route path="/team-members" component={TeamMembers} />
            <Route path="/time-ssheet" component={TimeSheet} />
            <Route path="/day" component={Day} />
            <Route path="/teams" component={Teams} />
            <Route path="/vacation" component={Vacation} />
            <Route path="/vac-timeline" component={Timeline} />
            
          </Router>
        </div>
      </div>
    );
  }
}

export default Content;

import React from "react";

class Footer extends React.Component {
  render() {
    return (
      <footer className="bg-dark footer">
        <p>Copyright @ 2013. VegaITSourcing All rights reserved</p>
        <a href="./tos">Terms of service</a>
        <a href="./pp">Privacy policy</a>
      </footer>
    );
  }
}
export default Footer;

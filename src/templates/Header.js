import React from "react";
import logo from "../logo.png";
import Login from "./Login";
import VacRequest from "../vacations/VacRequest";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      overlayHiden: true,
      overlayHidenVac: true,
      username: null,
      error: null,
      isLoaded: false,
      user: null
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/api/auth/loggedIn")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            username: result.username
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  logout = () => {
    fetch("http://localhost:8080/api/auth/logout", {
      method: "POST"
    })
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            username: result.username
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
    window.location.reload();
  };

  toggleOverlay = () => {
    this.setState({
      overlayHiden: !this.state.overlayHiden
    });
  };

  toggleOverlayVac = () => {
    this.setState({
      overlayHidenVac: !this.state.overlayHidenVac
    });
  };

  render() {
    var date = new Date();
    const day = date.getDate()
    const year = date.getFullYear();
    const m = +date.getMonth() + +1;

    const query = "?year=" + year + "&month="+ m +"&day=" + day;
    return (
      <header>
        {!this.state.overlayHiden && (
          <Login toggleOverlay={this.toggleOverlay} />
        )}
        {!this.state.overlayHidenVac && (
          <VacRequest toggleOverlayVac={this.toggleOverlayVac} />
        )}
        {(this.state.username === null ||
          this.state.username === undefined ||
          this.state.username === "anonymousUser") && (
          <div>
            <button
              type="button"
              onClick={this.toggleOverlay}
              className="btn btn-light headerButton"
            >
              Login
            </button>
          </div>
        )}

        {this.state.username !== null &&
          this.state.username !== undefined &&
          this.state.username !== "anonymousUser" && (
            <div>
              <p>{this.state.username}</p>
              <p onClick={this.logout}> | Logout</p>
              <button
                type="button"
                onClick={this.toggleOverlayVac}
                className="btn btn-light headerButton"
              >
                Vacation request
              </button>
            </div>
          )}
        <img className="logo" src={logo} alt="logo" />

        <a href="/time-ssheet">
          <button type="button" className="btn btn-light headerButton">
            TimeSheet
          </button>
        </a>

        <a href="/clients">
          <button type="button" className="btn btn-light headerButton">
            Clients
          </button>
        </a>
        <a href="/projects">
          <button type="button" className="btn btn-light headerButton">
            Projects
          </button>
        </a>
        <a href="/categories">
          <button type="button" className="btn btn-light headerButton">
            Categories
          </button>
        </a>
        <a href="/team-members">
          <button type="button" className="btn btn-light headerButton">
            Team members
          </button>
        </a>
        <a href="/reports">
          <button type="button" className="btn btn-light headerButton">
            Reports
          </button>
        </a>
        <a href="/teams">
          <button type="button" className="btn btn-light headerButton">
            Teams
          </button>
        </a>
        <a href={"/vac-timeline"+ query}>
          <button type="button" className="btn btn-light headerButton">
          Vacation timeline
          </button>
        </a>
      </header>
    );
  }
}

export default Header;

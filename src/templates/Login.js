import React from "react";

class NewProject extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,

      overlayHiden: true
    };
  }

  login = () => {
    fetch("http://localhost:8080/api/auth/login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        username: this.refs.username.value,
        password: this.refs.password.value
      })
    })
      .then(res => res.json())
      .then(result => {
        fetch("http://localhost:8080/api/auth/loggedIn")
          .then(res => res.json())
          .then(
            result => {
              this.setState({
                isLoaded: true,
                username: result.user
              });
            },
            error => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          );
        window.location.reload();
      });
  };

  render() {
    return (
      <div id="overlay">
        <div className="form-group overlay-From">
          <form className="mx-auto inputDiv">
            <h5>Login</h5>
            <div className="form-group">
              <label>Username</label>
              <input
                className="form-control"
                ref="username"
                placeholder="Username"
              />
            </div>
            <div className="form-group">
              <label>Password</label>
              <input
                className="form-control"
                ref="password"
                placeholder="Password"
              />
            </div>
            <button
              onClick={this.login}
              type="button"
              className="btn btn-success overlaybtn"
            >
              Add
            </button>
            <button
              onClick={this.props.toggleOverlay}
              type="button"
              className="btn btn-danger overlaybtn"
            >
              Cancel
            </button>
          </form>
        </div>
      </div>
    );
  }
}
export default NewProject;

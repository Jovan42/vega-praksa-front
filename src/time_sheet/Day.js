import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Work from "./Work";
import NewWork from "./NewWork";

class Day extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      works: null,
      totalHours: null,
      overlayHiden: true,
      letter: null,
      date: null
    };
  }

  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    const day = query.get("day");
    const month = query.get("month");
    const year = query.get("year");
    this.state.date = year + "-" + month + "-" + day;

    fetch(
      "http://localhost:8080/api/works/for-day" + this.props.location.search
    )
      .then(res => res.json())
      .then(
        result => {
          var hrs = 0;
          this.setState({
            isLoaded: true,
            works: result.works,
            totalHours: hrs
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  toggleOverlay = () => {
    this.setState({
      overlayHiden: !this.state.overlayHiden
    });
  };

  render() {
    const { error, isLoaded, works } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="content">
          {!this.state.overlayHiden && (
            <NewWork
              date={this.state.date}
              toggleOverlay={this.toggleOverlay}
            />
          )}

          <table>
          <tbody>
            <tr className="heading">
              <th align="left" width="150">
                Projects
              </th>
              <th align="left" width="150">
                Categories
              </th>
              <th align="left" width="150">
                Description
              </th>
              <th align="left" width="20">
                Time
              </th>
              <th align="left" width="20">
                OT
              </th>
            </tr>

            {works.map(item => (
              <Work key={item.id} date={this.state.date} item={item} />
            ))}
            </tbody>
          </table>

          <div className="float-left back">
            <a href="./time-stamp"> &#60;- Back to monhtly</a>
          </div>

          <div className="float-right">
            <button
              type="button"
              onClick={this.toggleOverlay}
              className="btn btn-success overlaybtn"
            >
              +
            </button>
          </div>
        </div>
      );
    }
  }
}

export default Day;

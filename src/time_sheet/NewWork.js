import React from "react";

class NewWork extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoadedCat: false,
      categories: null,
      isLoadedCli: false,
      isLoadedPM: false,
      projects: null,
      overlayHiden: true,
      members:null
    };
  }

  addWork = () => {
    var categoryId = this.refs.editCategory.options[
      this.refs.editCategory.selectedIndex
    ].value;
    var projectid = this.refs.editProject.options[
      this.refs.editProject.selectedIndex
    ].value;
    var employeeid = this.refs.editEmployee.options[
      this.refs.editEmployee.selectedIndex
    ].value;

    fetch("http://localhost:8080/api/works", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        description: this.refs.desc.value,
        time: this.refs.time.value,
        overtime: this.refs.overtime.value,
        date: this.props.date,
        category: categoryId ,
        project: projectid,
        employee: employeeid
      })
    });
    window.location.reload();
  };
  addCategory = () => {
    fetch("http://localhost:8080/api/categories", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: this.refs.name.value
      })
    });
    window.location.reload();
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/categories")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedCat: true,
            categories: result.categories
          });
        },
        error => {
          this.setState({
            isLoadedCat: true,
            error
          });
        }
      );

      fetch("http://localhost:8080/api/projects")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedCli: true,
            projects: result.projects
          });
        },
        error => {
          this.setState({
            isLoadedCli: true,
            error
          });
        }
      );
  }

  projectChanged = () => {
    var projectid = this.refs.editProject.options[
      this.refs.editProject.selectedIndex
    ].value;

    console.log("sad");

    fetch("http://localhost:8080/api/projects/" + projectid + "/members")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedPM: true,
            members: result.projectMembers
          });
        },
        error => {
          this.setState({
            isLoadedCli: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoadedCat, isLoadedCli, isLoadedPM } = this.state;

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoadedCat || !isLoadedCli ) {
      return <div>Loading...</div>;
    } else {
      return (
        <div id="overlay">
          <div className="form-group overlay-From">
            <form className="mx-auto inputDiv">
              <div className="">
                <label>Description</label>
                <input
                  ref="desc"
                  className="mx-auto form-control"
                  placeholder="Description"
                />
                <label>Time</label>
                <input
                  ref="time"
                  className="mx-auto form-control"
                  placeholder="Time"
                />
                <label>Overtime</label>
                <input
                  ref="overtime"
                  className="mx-auto form-control"
                  placeholder="Overtime"
                />
                <div className="form-group">
                  <label className="radioLabel">Categories</label>
                  <select ref="editCategory" className="form-control" >
                    {this.state.categories.map(item => (
                      <option value={item.id}>{item.name}</option>
                    ))}
                  </select>{" "}
                </div>
                <label className="radioLabel">Projects</label>
                <select ref="editProject"  className="form-control" onChange={this.projectChanged}>
                  <option value="null"></option>
                  {this.state.projects.map(item => (
                    <option value={item.id}>{item.name}</option>
                  ))}
                </select>
                  { isLoadedPM &&
                  <div className="form-group">
                    <label className="radioLabel">Employee</label>
                    <select ref="editEmployee" className="form-control">
                      {this.state.members.map(item => (
                        <option value={item.employee.id}>{item.employee.name}</option>
                      ))}
                    </select>{" "}
                      </div> }
              </div> 
              <button
                onClick={this.addWork}
                type="button"
                className="btn btn-success overlaybtn"
              >
                Add
              </button>
              <button
                onClick={this.props.toggleOverlay}
                type="button"
                className="btn btn-danger overlaybtn"
              >
                Cancel
              </button>
            </form>
          </div>
        </div>
      );
    }
  }
}
export default NewWork;

import React from "react";

class Work extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      projects: null,
      clients: null,
      categories: null,
      isLoadedCl: false,
      isLoadedPrL: false,
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/api/clients")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedCl: true,
            clients: result.clients
          });
        },
        error => {
          this.setState({
            isLoadedCl: true,
            error
          });
        }
      );

    fetch("http://localhost:8080/api/projects")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedPr: true,
            projects: result.projects
          });
        },
        error => {
          this.setState({
            isLoadedPr: true,
            error
          });
        }
      );

    fetch("http://localhost:8080/api/categories")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedCat: true,
            categories: result.categories
          });
        },
        error => {
          this.setState({
            isLoadedCat: true,
            error
          });
        }
      );
  }

  delete = () => {
    fetch("http://localhost:8080/api/works/" + this.props.item.id, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    });
  };

  save = () => {
    var categoryId = this.refs.editCategory.options[
      this.refs.editCategory.selectedIndex
    ].value;
    var projectid = this.refs.editProject.options[
      this.refs.editProject.selectedIndex
    ].value;

    fetch("http://localhost:8080/api/works", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id: this.props.item.id,
        description: this.refs.editDsc.value,
        time: this.refs.editTime.value,
        overtime: this.refs.editOvertime.value,
        date: this.props.date,
        category: {
          id: categoryId
        },
        project: {
          id: projectid
        }
      })
    });

    window.location.reload();
  };

  render() {
    if (this.props.item !== "undefined") {
      var item = this.props.item;
    }
    const {
      error,
      isLoadedCl,
      isLoadedPr,
      isLoadedCat,
    } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoadedCl || !isLoadedPr || !isLoadedCat) {
      return <tr><td>Loading...</td></tr>;
    } else {
      return (
        <tr>
          <td width="150" align="left">
            <select defaultValue={this.props.item.project.name} className="drop" ref="editProject">
              <option defaultValue="null"/>
              {this.state.projects.map(item => (
                <option
                  key={item.id}
                  defaultValue={item.id}
                >
                  {item.name}
                </option>
              ))}
            </select>
          </td>
          <td width="150" align="left">
            <select defaultValue={this.props.item.category.name} className="drop" ref="editCategory">
              <option defaultValue="null" />
              {this.state.categories.map(item => (
                <option
                  key={item.id}
                  defaultValue={item.id}
                >
                  {item.name}
                </option>
              ))}
            </select>
          </td>
          <td width="150" align="left">
            <input
              className="drop"
              defaultValue={item.description}
              type="text"
              ref="editDsc"
            />
          </td>
          <td width="20" align="left">
            <input
              className="time text-center"
              defaultValue={item.time}
              type="text"
              ref="editTime"
            />
          </td>

          <td width="20" align="left">
            <input
              className="time text-center"
              defaultValue={item.overtime}
              type="text"
              ref="editOvertime"
            />
          </td>
          <td width="20" align="left">
            <button
              className="sd btn btn-success"
              onClick={this.save}
              defaultValue={item.time}
              type="text"
            >
              S
            </button>
          </td>
          <td width="20" align="left">
            <button
              className="btn btn-danger sd"
              onClick={this.delete}
              defaultValue={item.time}
              type="text"
            >
              D
            </button>
          </td>
        </tr>
      );
    }
  }
}
export default Work;

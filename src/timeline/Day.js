import React, { Component } from "react";
import "../timeline.css";
import "bootstrap/dist/css/bootstrap.min.css";

class Day extends Component {
 
  click = () => {
    console.log(this.props.vacation)
    if(this.props.vacation === null) return;
    else {
      window.location =
      "/vacation?vacation=" + this.props.vacation.id
    }
  }
  render() {
      var fill = "";
      if (this.props.vacation !== null) 
      {
        fill = "day-fill" 
        console.log(this.props.vacation.approved)
        if (this.props.vacation.approved === true) fill = "approved"
        else if (this.props.vacation.approved === false) fill = "denied"
         
      }  
      return <th  onClick={this.click} className={"day "+ fill}></th>;
  }
}

export default Day;

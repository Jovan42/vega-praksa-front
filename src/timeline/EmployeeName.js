import React, { Component } from "react";
import "../timeline.css";
import "bootstrap/dist/css/bootstrap.min.css";

class EmployeeName extends Component {
  render() {
    return <th className="employee-name">{this.props.name}</th>;
  }
}

export default EmployeeName;

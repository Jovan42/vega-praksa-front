import React, { Component } from "react";
import "../timeline.css";
import "bootstrap/dist/css/bootstrap.min.css";

class Day extends Component {
 
  getDateString = ( date) => {
    var m = +date.getMonth() + +1;
      var r = (date.getDate() + ". " + m + ". " + date.getFullYear() + ".")
        date.setDate(date.getDate() + 1);
        return r;
    } 

  render() {
      var d = new Date(this.props.startDate.valueOf());
    return (
      <tr>
        <th className="employee-name" />
        <th className="day">{this.getDateString(d)}</th>
        <th className="day">{this.getDateString(d)}</th>
        <th className="day">{this.getDateString(d)}</th>
        <th className="day">{this.getDateString(d)}</th>
        <th className="day">{this.getDateString(d)}</th>
        <th className="day">{this.getDateString(d)}</th>
        <th className="day">{this.getDateString(d)}</th>
      </tr>
    );
  }
}

export default Day;

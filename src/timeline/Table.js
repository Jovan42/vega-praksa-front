import React, { Component } from "react";
import "../timeline.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Week from "./Week";
import Header from "./Header";


class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            employees: null,
          };
      }
    
      componentDidMount() {
        fetch("http://localhost:8080/api/team-members")
          .then(res => res.json())
          .then(
            result => {
              this.setState({
                isLoaded: true,
                employees: result.teamMembers
              });
            },
            error => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          );
      }
  render() {
    const { error, isLoaded} = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <table>
          <Header startDate={this.props.startDate} />
          {this.state.employees.map(employee => (
             <Week key={employee.id}
             startDate={this.props.startDate} 
             employee={employee}
           />
          ))}
        </table>
      );
    }
  }
}

export default Table;

import React from "react";
import Table from "./Table";

class Timeline extends React.Component {
  previous = () => {
    const query = new URLSearchParams(this.props.location.search);
    const day = query.get("day");
    var month = query.get("month");
    const year = query.get("year");
    var date = new Date(year + "-" + month + "-" + day);
    date.setDate(date.getDate() - 7);
    const m = +date.getMonth() + +1;

    window.location =
      "/vac-timeline?year=" +
      date.getFullYear() +
      "&month=" +
      m +
      "&day=" +
      date.getDate();
  };

  previousDay = () => {
    const query = new URLSearchParams(this.props.location.search);
    const day = query.get("day");
    var month = query.get("month");
    const year = query.get("year");
    var date = new Date(year + "-" + month + "-" + day);
    date.setDate(date.getDate() - 1);
    const m = +date.getMonth() + +1;

    window.location =
      "/vac-timeline?year=" +
      date.getFullYear() +
      "&month=" +
      m +
      "&day=" +
      date.getDate();
  };
  
  next = () => {
    const query = new URLSearchParams(this.props.location.search);
    const day = query.get("day");
    var month = query.get("month");
    const year = query.get("year");

    var date = new Date(year + "-" + month + "-" + day);
    date.setDate(date.getDate() + 7);
    const m = +date.getMonth() + +1;
    window.location =
      "/vac-timeline?year=" +
      date.getFullYear() +
      "&month=" +
      m +
      "&day=" +
      date.getDate();
  };

  
  nextDay = () => {
    const query = new URLSearchParams(this.props.location.search);
    const day = query.get("day");
    var month = query.get("month");
    const year = query.get("year");

    var date = new Date(year + "-" + month + "-" + day);
    date.setDate(date.getDate() + 1);
    const m = +date.getMonth() + +1;
    window.location =
      "/vac-timeline?year=" +
      date.getFullYear() +
      "&month=" +
      m +
      "&day=" +
      date.getDate();
  };

  jumpTo = () => {
      var date = new Date(this.refs.date.value)
      const m = +date.getMonth() + +1;
      window.location =
        "/vac-timeline?year=" +
        date.getFullYear() +
        "&month=" +
        m +
        "&day=" +
        date.getDate();
  }

  render() {
    const query = new URLSearchParams(this.props.location.search);
    var day = query.get("day");
    if (day < 10) day = "0" + day;
    var month = query.get("month");
    if (month < 10) month = "0" + month;      
    const year = query.get("year");      
    var date = year + "-" + month + "-" + day;

    
    return (
      <div>
        <div>
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.previous}
          >
            &lt;&lt;
          </button>
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.previousDay}
          >
            &lt;
          </button>
          <div className="inputDate">
            <input ref="date" defaultValue={date} className="form-control" type="date" />
            <button type="button" className="btn btn-primary" onClick={this.jumpTo}>
            Jump to
          </button>
          </div>
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.nextDay}
          >
            &gt;
          </button>
          <button type="button" className="btn btn-primary" onClick={this.next}>
            &gt;&gt;
          </button>
        </div>
        <Table startDate={new Date(date)} />
      </div>
    );
  }
}

export default Timeline;

import React, { Component } from "react";
import "../timeline.css";
import Day from "./Day";
import EmployeeName from "./EmployeeName";

import "bootstrap/dist/css/bootstrap.min.css";

class Week extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      vacations: null,
      startDate: this.props.startDate,
      arary: [null, null, null, null, null, null, null]
    };
  }
  getDateString = date => {
    var m = +date.getMonth() + +1;
    var r = date.getFullYear() + "-" + m + "-" + date.getDate();
    date.setDate(date.getDate() + 7);
    return r;
  };

  componentDidMount() {
    var date = new Date(this.state.startDate.valueOf());
    var startDate = this.getDateString(date);
    var endDate = this.getDateString(date);
    fetch(
      "http://localhost:8080/api/vacations/for-timeline?employee=" +
        this.props.employee.id +
        "&startDate=" +
        startDate +
        "&endDate=" +
        endDate
    )
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            vacations: result.vacations
          });

        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );

  }
  vacationsToArray = vacations => {
    var array = this.state.arary;
    var date = new Date(this.state.startDate.valueOf());
    var filled=false;

    vacations.map(vacation => {
      var vacationStartDate = new Date(vacation.startDate);
      var endDate = new Date(vacation.endDate)
      filled = false;
      var numberOfDays = endDate.getDate() - vacationStartDate.getDate();
      for (var i = 0; i < 7; i++) {
        if (vacationStartDate.getDate() === date.getDate()) {
          for (var k = 0; k < numberOfDays; k++) {
            if (i === 7) break;
            array[i] = vacation;
            i++;
          }
          filled = true;
        } else if(vacationStartDate.getDate() <= date.getDate() && !filled) {
            const diference =date.getDate() - vacationStartDate.getDate(); 
            for (k = 0; k < numberOfDays - diference; k++) {
                if (i === 7) break;
                array[i] = vacation;
                i++;
              }
          filled = true;

        }
        date.setDate(date.getDate() + 1);
      }
      return true;
    });

    return array;
  };

  render() {
    const { error, isLoaded} = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
    this.vacationsToArray(this.state.vacations);
      return (
        <tr>
          <EmployeeName name={this.props.employee.name} />
          {this.state.arary.map(fill => (
            <Day vacation={fill}/>
          ))}
        </tr>
      );
    }
  }
}

export default Week;

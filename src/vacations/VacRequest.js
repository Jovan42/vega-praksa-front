import React from "react";

class VacRequest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,

      overlayHiden: true
    };
  }

  request = () => {
    fetch("http://localhost:8080/api/vacations/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        startDate: this.refs.startDate.value,
        endDate: this.refs.endDate.value,
        employee: 1
      })
    });

    window.location.reload();
  };

  render() {
    return (
      <div id="overlay">
        <div className="form-group overlay-From">
          <form className="mx-auto inputDiv">
            <h5>Login</h5>
            <div className="form-group">
              <label>Start date </label>
              <input
                type="date"
                className="form-control"
                ref="startDate"
                placeholder="Date"
              />
            </div>
            <div className="form-group">
              <label>End date </label>
              <input
                 type="date"
                 className="form-control"
                 ref="endDate"
                 placeholder="Date"
              />
            </div>
            <button
              onClick={this.request}
              type="button"
              className="btn btn-success overlaybtn"
            >
              Request
            </button>
            <button
              onClick={this.props.toggleOverlayVac}
              type="button"
              className="btn btn-danger overlaybtn"
            >
              Cancel
            </button>
          </form>
        </div>
      </div>
    );
  }
}
export default VacRequest;

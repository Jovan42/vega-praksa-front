import React from "react";
import ViewVacation from "./ViewVacation";

class Cateogry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      vacation: null,
      isLoaded: false
    };
  }

  toggleEdit = () => {
    this.setState({
      edit: !this.state.edit
    });
  };
  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    const vacationId = query.get("vacation");
    fetch("http://localhost:8080/api/vacations/" + vacationId)
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            vacation: result
          });
          console.log(this.state.vacation);
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }
  render() {
    const { error, isLoaded } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      var approcedClass = "";
      var disable = "";
      if (this.state.vacation.approved === false) {
        disable = "disable";
        approcedClass = "bg-danger";
      } else if (this.state.vacation.approved === true) {
        disable = "disable";
        approcedClass = "bg-success";
      }
      return <ViewVacation approcedClass= {approcedClass} disable={disable} item={this.state.vacation} />;
    }
  }
}
export default Cateogry;

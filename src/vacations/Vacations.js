import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Vacation from "./Vacation";

class Vacations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      vacations: null,
      overlayHiden: true
    };
  }

  toggleOverlay = () => {
    this.setState({
      overlayHiden: !this.state.overlayHiden
    });
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/vacations")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            vacations: result.vacations
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, vacations } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="content" >
          
          <div>
            {vacations.map(item => (
              <Vacation  key={item.id}item={item} />
            ))}
          </div>
        </div>
      );
    }
  }
}

export default Vacations;

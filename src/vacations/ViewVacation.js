import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

class ViewVacation extends React.Component {
  

  approve = () => {
    fetch("http://localhost:8080/api/vacations/" + this.props.item.id + "/approve", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
    });
    window.location.reload();
  };

  deny = () => {
    fetch("http://localhost:8080/api/vacations/" + this.props.item.id + "/deny", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({})
    });
    window.location.reload();
  };


  render() {
    var date = new Date(this.props.item.startDate); 
    return (
      <div >
        <form>
          <h2 className= {this.props.approcedClass}>{this.props.item.employee.name}</h2>
          <div className="form-group">
            <label>Starting date</label>
            <input
             disabled
              defaultValue={date}
              className="form-control"
              ref="editName"
              placeholder="Enter name"
            />
          </div>
          <div className="form-group">
            <label>Number of days</label>
            <input
             disabled
              defaultValue={this.props.item.numberOfDays}
              className="form-control"
              ref="editName"
              placeholder="Enter name"
            />
          </div>
          <button
            disabled = {this.props.disable}
            onClick={this.approve}
            type="button"
            className="btn btn-success overlaybtn"
          >
            Approve
          </button>
          <button
            disabled = {this.props.disable}
            onClick={this.deny}
            type="button"
            className="btn btn-danger overlaybtn"
          >
            Deny
          </button>
        </form>
      </div>
    );
  }
}
export default ViewVacation;

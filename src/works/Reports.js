import React from "react";
import Work from "./Work";
import SearchFrom from "./SearchForm";

class Reports extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      works: null,
      overlayHiden: true,
      totalHours: null,
      letter: null
    };
  }

  search = searchString => {
      console.log("http://localhost:8080/api/works/search" + searchString)
    fetch("http://localhost:8080/api/works/search" + searchString)
      .then(res => res.json())
      .then(
        result => {
            var hrs = 0;
            result.works.map(item => {
              return hrs += item.time;
            });
          this.setState({
            isLoaded: true,
            works: result.works,
            totalHours: hrs
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/works/search")
      .then(res => res.json())
      .then(
        result => {
          var hrs = 0;
          result.works.map(item => {
            return hrs += item.time;
          });

          this.setState({
            isLoaded: true,
            works: result.works,
            totalHours: hrs
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    const { error, isLoaded, works, totalHours } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="content">
          <SearchFrom search={this.search} />
          <table>
            <tbody>
              <tr className="heading">
                <th align="left" width="120">
                  Date
                </th>
                <th align="left" width="150">
                  Employee
                </th>
                <th align="left" width="150">
                  Projects
                </th>
                <th align="left" width="150">
                  Categories
                </th>
                <th align="left" width="150">
                  Description
                </th>
                <th align="left" width="20">
                  Time
                </th>
              </tr>
              {works.map(item => (
                <Work key={item.id} item={item} />
              ))}
            </tbody>
          </table>

          <div className="totalHrs">
            <label className="inline">Report Total: </label>
            <p className="inline"> {totalHours}</p>
          </div>
        </div>
      );
    }
  }
}

export default Reports;

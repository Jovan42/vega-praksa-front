import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

class SearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoadedTM: false,
      isLoadedCl: false,
      isLoadedPr: false,
      isLoadedCat: false,
      teamMembers: null,
      clients: null,
      projects: null,
      categories: null,
      overlayHiden: true,
      letter: null
    };
  }

  reset = () => {
    /*this.refs.editTeamMember.value = null;
    this.refs.editCategory.value = null;
    this.refs.editClient.value = null;
    this.refs.editProject.value = null;*/
    window.location.reload();
  };

  getSearchString = () => {
    var searchString = "?";

    if (this.refs.editTeamMember.value !== "null")
      searchString += "lead=" + this.refs.editTeamMember.value + "&";
    if (this.refs.editClient.value !== "null")
      searchString += "client=" + this.refs.editClient.value + "&";
    if (this.refs.editProject.value !== "null")
      searchString += "project=" + this.refs.editProject.value + "&";
    if (this.refs.editCategory.value !== "null")
      searchString += "category=" + this.refs.editCategory.value + "&";
    if (this.refs.editStartDate.value !== "null")
      searchString += "startDate=" + this.refs.editStartDate.value + "&";
    if (this.refs.editEndDate.value !== "null")
      searchString += "endDate=" + this.refs.editEndDate.value + "&";
    console.log(searchString)
    this.props.search(searchString);
  };

  componentDidMount() {
    fetch("http://localhost:8080/api/team-members")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedTM: true,
            teamMembers: result.teamMembers
          });
        },
        error => {
          this.setState({
            isLoadedTM: true,
            error
          });
        }
      );

    fetch("http://localhost:8080/api/clients")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedCl: true,
            clients: result.clients
          });
        },
        error => {
          this.setState({
            isLoadedCl: true,
            error
          });
        }
      );

    fetch("http://localhost:8080/api/projects")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedPr: true,
            projects: result.projects
          });
        },
        error => {
          this.setState({
            isLoadedPr: true,
            error
          });
        }
      );

    fetch("http://localhost:8080/api/categories")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoadedCat: true,
            categories: result.categories
          });
        },
        error => {
          this.setState({
            isLoadedCat: true,
            error
          });
        }
      );
  }

  render() {
    const {
      error,
      isLoadedTM,
      isLoadedCl,
      isLoadedPr,
      isLoadedCat,
      clients,
      projects,
      categories
    } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoadedTM || !isLoadedCl || !isLoadedPr || !isLoadedCat) {
      return <div>Loading...</div>;
    } else {
      return (
        <form className="workSearch">
          <div className="row">
            <div className="col">
              <label>Team Member</label>
              <select type="text" className="form-control" ref="editTeamMember">
                <option value="null">All</option>
                {this.state.teamMembers.map(item => (
                  <option key={item.id} value={item.id}>{item.name}</option>
                ))}
              </select>
            </div>
            <div className="col">
              <label>Client</label>
              <select type="text" className="form-control" ref="editClient">
                <option value="null">All</option>
                {clients.map(item => (
                  <option  key={item.id}  value={item.id}>{item.name}</option>
                ))}
              </select>
            </div>
            <div className="col">
              <label>Project</label>
              <select type="text" className="form-control" ref="editProject">
                <option value="null">All</option>
                {projects.map(item => (
                  <option  key={item.id}  value={item.id}>{item.name}</option>
                ))}
              </select>
            </div>
          </div>
          <div className="row">
            <div className="col">
              <label>Category</label>
              <select type="text" className="form-control" ref="editCategory">
                <option value="null">All</option>
                {categories.map(item => (
                  <option  key={item.id}  value={item.id}>{item.name}</option>
                ))}
              </select>
            </div>
            <div className="col">
              <label>Start Date</label>
              <input type="date" className="form-control" ref="editStartDate" />
            </div>
            <div className="col">
              <label>End date</label>
              <input type="date" className="form-control" ref="editEndDate" />
            </div>
          </div>
          <button
            onClick={this.getSearchString}
            type="button"
            className="btn btn-success overlaybtn"
          >
            Search
          </button>
          <button
            onClick={this.reset}
            type="button"
            className="btn btn-danger overlaybtn"
          >
            Reset
          </button>
        </form>
      );
    }
  }
}

export default SearchForm;

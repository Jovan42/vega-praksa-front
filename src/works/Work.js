import React from "react";

class Work extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false
    };
  }

  render() {
    console.log(this.props.item)
    return (
      <tr>
        <td width="120" align="left">
          {" "}
          {this.props.item.date}
        </td>
        <td width="150" align="left">
          {this.props.item.employee.name}
        </td>
        <td width="150" align="left">
          {this.props.item.project.name}{" "}
        </td>
        <td width="150" align="left">
          {this.props.item.category.name}
        </td>
        <td width="150" align="left">
          {" "}
          {this.props.item.description}
        </td>
        <td width="20" align="left">
          {this.props.item.time}{" "}
        </td>
      </tr>
    );
  }
}
export default Work;
